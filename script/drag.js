var dragged
document.addEventListener('dragstart', event => {
  event.dataTransfer.setData('text/plain', null)  // FireFox
  dragged = event.target
  setTimeout(() => event.target.style.visibility = "hidden", 1)
})
document.addEventListener('dragend', event => {
  event.target.style.visibility = ""
})
document.addEventListener('dragenter', event => {
  event.preventDefault()
})
document.addEventListener('dragover', event => {
  event.preventDefault()
})
document.addEventListener('drop', event => {
  if (event.target.classList.contains("tower") && dragged) {
    ports.toMove.send([dragged.id, event.target.id])
  }
})