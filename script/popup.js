const Popup = c => swal(`${c} Steps!`, "", "success", { button: "Replay?" }).then(x => ports.toRestart.send(Boolean(x)))
ports.toPopup.subscribe(Popup)