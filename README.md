# Hanoi

A simple Hanoi game powered by Elm!

Design Decision:

- Readable.
- Responsive.
- Let Elm handle the main structure and application logic. No Runtime Error!
- Let Javascript snippets handle additional effects.

Used Framework:

- Elm
- SweetAlert.js for Popup Window

Nothing more.