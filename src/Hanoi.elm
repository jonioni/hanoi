port module Hanoi exposing (main)

import Task
import Html as Vdom
import Html.Attributes as Attributes
import Html.Events as Events
import Browser


--- Model ---


type Plate = Bottom | Middle | Top

type Position = Left | Center | Right

type alias Model = { bottom : Position, middle : Position, top : Position, count : Int }

init : Model
init = { bottom = Left, middle = Left, top = Left, count = 0 }


--- Helper ---


plateToId : Plate -> String
plateToId plate = case plate of
    Bottom -> "bottom"
    Middle -> "middle"
    Top -> "top"

idToPlate : String -> Maybe Plate
idToPlate id = case id of
    "bottom" -> Just Bottom
    "middle" -> Just Middle
    "top" -> Just Top
    _ -> Nothing

positionToId : Position -> String
positionToId position = case position of
    Left -> "left"
    Center -> "center"
    Right -> "right"

idToPosition : String -> Maybe Position
idToPosition id = case id of
    "left" -> Just Left
    "center" -> Just Center
    "right" -> Just Right
    _ -> Nothing

getPlatesAtPosition : Position -> Model -> List Plate
getPlatesAtPosition position state = List.filterMap identity <|
    [
        if state.top == position then Just Top else Nothing,
        if state.middle == position then Just Middle else Nothing,
        if state.bottom == position then Just Bottom else Nothing
    ]

finished : Model -> Bool
finished state = (state.bottom == Right) && (state.middle == Right) && (state.top == Right)


--- Action ---


type Action = Move (Plate, Position)
            | CheckFinish
            | Restart Bool
            | InvalidAction

send : Action -> Cmd Action
send action = Task.succeed action |> Task.perform identity

update : Action -> Model -> (Model, Cmd Action)
update action state =
    case action of
        Move (plate, newPosition) ->
            if moveIsValid (plate, newPosition) state then
                case plate of
                    Bottom -> ({ state | bottom = newPosition, count = state.count + 1 }, send CheckFinish)
                    Middle -> ({ state | middle = newPosition, count = state.count + 1 }, send CheckFinish)
                    Top -> ({ state | top = newPosition, count = state.count + 1 }, send CheckFinish)
            else
                (state, Cmd.none)
        CheckFinish ->
                if finished state then (state, toPopup (state.count)) else (state, Cmd.none)
        Restart agree ->
            if agree then (init, Cmd.none) else (state, Cmd.none)
        InvalidAction ->
            (state, Cmd.none)

moveIsValid : (Plate, Position) -> Model -> Bool
moveIsValid (plate, newPosition) state =
    case plate of
        Bottom ->
            (.bottom state /= .middle state) && (.bottom state /= .top state) &&
            (newPosition /= .middle state) && (newPosition /= .top state)
        Middle ->
            (.middle state /= .top state) && (newPosition /= .top state)
        Top ->
            True


--- View ---


view : Model -> Vdom.Html Action
view state = Vdom.div [ Attributes.id "hanoi" ]
    [
        viewTower Left state,
        viewTower Center state,
        viewTower Right state
    ]

viewTower : Position -> Model -> Vdom.Html Action
viewTower position state = Vdom.div [ Attributes.id <| positionToId position, Attributes.class "tower" ]
    <| [ viewTowerBar ] ++ List.indexedMap viewPlate (getPlatesAtPosition position state)

viewTowerBar : Vdom.Html Action
viewTowerBar = Vdom.div [ Attributes.class "tower-bar" ] []

viewPlate : Int -> Plate -> Vdom.Html Action
viewPlate index plate = Vdom.div
    [
        Attributes.id <| plateToId plate, Attributes.class "plate",
        Attributes.attribute "draggable" (if index == 0 then "true" else "false")
    ] []


--- Subscription ---


port toPopup : Int -> Cmd a
port toMove : (List String -> a) -> Sub a
port toRestart : (Bool -> a) -> Sub a

moveByArguments : List String -> Action
moveByArguments arguments = case arguments of
    plateId::positionId::_ -> moveByIds (plateId, positionId)
    _ -> InvalidAction

moveByIds : (String, String) -> Action
moveByIds (plateId, positionId) = case (idToPlate plateId, idToPosition positionId) of
    (Just plate, Just position) -> Move (plate, position)
    (_, _) -> InvalidAction

subscriptions : Model -> Sub Action
subscriptions state = Sub.batch [ toRestart Restart, toMove moveByArguments ]


--- VM ---


main : Program () Model Action
main = Browser.element { init = \flags -> (init, Cmd.none), update = update, view = view, subscriptions = subscriptions }